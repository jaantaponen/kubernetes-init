#!/bin/bash
rm -rf kubernetes-init
git clone https://gitlab.com/janetsqy/kubernetes-init.git
chmod +x kubernetes-init/scripts/kubernetesCore.sh
chmod +x kubernetes-init/scripts/k3sinit.sh
sudo apt update &>/dev/null
sudo apt install figlet -y &>/dev/null
clear

figlet "Jaan Taponen KUBERNETES"

k3sinit() {
    clear
    figlet k3s init
    PS3='Please enter your choice: '
    options=("Default k3s" "k3s with metal+flannel" "Quit")
    select opt in "${options[@]}"
    do
        case $opt in
            "Default k3s")
                echo "Launching default installer"
                bash kubernetes-init/scripts/k3sinit.sh default
                break
                ;;
            "k3s with metal+flannel")
                echo "Lauching k3s with metal+flannel"
                bash kubernetes-init/scripts/k3sinit.sh minimum
                break
                ;;
            "Quit")
                break
                ;;
            *) echo "invalid option $REPLY";;
        esac
    done
}



kube-core() {
    # Evaluating the user inputs before continuing into the installation script.
    clear
    figlet Kubernetes Core
    param1=no
    while true; do
        read -p "Do you want to update your system [Yy/Nn]  " yn </dev/tty
        case $yn in
            [Yy]* ) param1=update; break;;
            [Nn]* ) sudo rm -rf kubernetes-init && exit;;
            * ) echo "Please answer yes or no.";;
        esac
    done

    param2=no
    if [ $param=update ]; then
        while true; do
            read -p "Install as master node? [Yy/Nn]  " yn </dev/tty
            case $yn in
                [Yy]* ) param2=master; break;;
                [Nn]* ) break;;
                * ) echo "Please answer yes or no.";;
            esac
        done
    fi

    param3=no
    while true; do
        read -p "Install gitlab admin-user? [Yy/Nn]  " yn </dev/tty
        case $yn in
            [Yy]* ) param3=gitlab; break;;
            [Nn]* ) break;;
            * ) echo "Please answer yes or no.";;
        esac
    done

    echo ""
    echo "You have chosen to bootsrap with the following:"
    if [ $param1 = update ]; then
        echo " --Update"
    fi

    if [ $param2 = master ]; then
        echo " --Install as master node"
    fi

    if [ $param3 = gitlab ]; then
        echo " --Gitlab-admin"
    fi

    while true; do
        read -p "Do you wish to continue? [Yy/Nn]  " yn </dev/tty
        case $yn in
            [Yy]* ) bash kubernetes-init/scripts/kubernetesCore.sh $param1 $param2 $param3; break;;
            [Nn]* ) rm -rf kubernetes-init && exit;;
            * ) echo "Please answer yes or no.";;
        esac
    done
}


PS3='Please enter your choice: '
options=("KubernetesCore" "k3s" "microk8s" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "KubernetesCore")
            echo "Launching KubernetesCore"
            kube-core
            break
            ;;
        "k3s")
            echo "Lauching k3s installer"
            k3sinit
            break
            ;;
        "microk8s")
            echo "Lauching microk8s installer"
            bash kubernetes-init/scripts/microk8s.sh
            break
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done


rm -rf kubernetes-init