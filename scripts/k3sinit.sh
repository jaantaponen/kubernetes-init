minimum() {  
    clear
    osote=$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1' | grep -v '172.17.0.1' | grep -v '10.42.0.0'| grep -v '10.42.0.1')
    curl -sfL https://get.k3s.io | sh -s - --no-deploy=servicelb --no-deploy=traefik --bind-address $osote
    kubectl label node $(hostname) kubernetes.io/role=master
    wait
    kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
    wait
    kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.8.0/manifests/metallb.yaml
    wait
    kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.8.0/manifests/example-layer2-config.yaml
    wait
    kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.8.0/manifests/tutorial-2.yaml
    wait
    kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml
    wait
    clear
    figlet "Lightweight 's the best way"
    echo ""
    echo "Please run the command down below on you node: "
    echo ""
    echo "curl -sfL https://get.k3s.io | K3S_URL=https://$osote:6443 K3S_TOKEN=$(sudo cat /var/lib/rancher/k3s/server/node-token) sh -" > creds.txt
    cat creds.txt
    gitlab-prompt
}

wait() {
    sleep 5
    while true; do
        wait=$(kubectl get nodes | grep $(hostname) |  awk '{print $2;}')
        if [ $wait = Ready ]; then
            break
        fi
        sleep 1
    done
}

gitlab-prompt() {
    while true; do
        read -p "Install gitlab admin user?[Yy/Nn]  " yn </dev/tty
        case $yn in
            [Yy]* ) gitlab; break;;
            [Nn]* ) exit;;
            * ) echo "Please answer yes or no.";;
        esac
    done
}

default() {
    osote=$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1' | grep -v '172.17.0.1')
    clear
    curl -sfL https://get.k3s.io | sh -
    kubectl label node $(hostname) kubernetes.io/role=master
    figlet "YOU DID IT BOI"
    echo ""
    echo "Please run the command down below on you node: "
    echo ""
    echo "curl -sfL https://get.k3s.io | K3S_URL=https://$osote:6443 K3S_TOKEN=$(sudo cat /var/lib/rancher/k3s/server/node-token) sh -" > creds.txt
    cat creds.txt
    gitlab-prompt
}

gitlab() {
    kubectl apply -f kubernetes-init/scripts/gitlab-admin-service-account.yaml
    kubectl get secret $(sudo kubectl get secrets | grep default-token |  awk '{print $1;}') -o jsonpath="{['data']['ca\.crt']}" | base64 --decode > gitlab-creds.txt
    kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}') > temp.txt
    cat temp.txt | grep token: >> gitlab-creds.txt
    rm temp.txt
    echo "*****GitLab******"
    echo ""
    cat gitlab-creds.txt
    echo ""
    echo "/*****GitLab******"
}

if [ $1 = minimum ]; then
    echo " --Chosen minimum"
    minimum
fi

if [ $1 = default ]; then
    echo " --Chosen default"
    default
fi