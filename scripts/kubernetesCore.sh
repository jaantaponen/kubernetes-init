#!/bin/bash
cluster() {
    clear
    osote=$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1' | grep -v '172.17.0.1')
    sudo kubeadm init --apiserver-advertise-address=$osote --pod-network-cidr=10.244.0.0/16 > $HOME/temp.txt
    sudo tail -2 $HOME/temp.txt > $HOME/joining_addr.txt
    rm $HOME/temp.txt
    mkdir -p $HOME/.kube
    sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
    sudo chown $(id -u):$(id -g) $HOME/.kube/config
    echo 'Waiting for Master to boot up...'
    sleep 20
    sudo kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
    echo 'Waiting for the network to boot up...'
    wait
    kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard.yaml
    echo 'Waiting for dashboard to boot up...'
    wait
    kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.7.3/manifests/metallb.yaml
    echo 'Waiting for metal to boot up...'
    wait
    kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.7.3/manifests/example-layer2-config.yaml
    sleep 8
    kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.7.3/manifests/tutorial-2.yaml
    clear
    echo "congratulations your Master node is running on $osote"
}

wait() {
    sleep 5
    while true; do
        wait=$(sudo kubectl get nodes | grep $(hostname) |  awk '{print $2;}')
        if [ $wait = Ready ]; then
            break
        fi
        sleep 1
    done
}

update() {
    echo 'checking for existing old docker files...'
    sudo apt-get remove docker docker-engine docker.io containerd runc
    # Updating system
    echo 'updating system and installing prequisites...'
    sudo apt update && sudo apt upgrade -y && sudo apt install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
    # Adding gpg keys to the system
    echo 'Adding GPG -keys...'
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
    stable"
    wget https://packages.cloud.google.com/apt/doc/apt-key.gpg
    sudo apt-key add apt-key.gpg
    sudo apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"
    sudo rm apt-key.gpg
    sudo apt-get update
    # Installing the required software
    echo 'Installing Docker and kubectl...'
    sudo apt install -y docker-ce docker-ce-cli containerd.io kubelet kubeadm kubectl
    # Starting services...
    sudo systemctl enable docker && sudo systemctl start docker
    sudo systemctl enable kubelet && sudo systemctl start kubelet
    
    echo 'Disabling swap'
    sudo swapoff -a
}

gitlab() {
    sudo kubectl apply -f kubernetes-init/scripts/gitlab-admin-service-account.yaml
    sudo kubectl get secret $(sudo kubectl get secrets | grep default-token |  awk '{print $1;}') -o jsonpath="{['data']['ca\.crt']}" | base64 --decode > gitlab-creds.txt
    sudo kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}') > temp.txt
    cat temp.txt | grep token: >> gitlab-creds.txt
    rm temp.txt
    echo "*****GitLab******"
    echo ""
    cat gitlab-creds.txt
    echo ""
    echo "/*****GitLab******"
}

finish() {
    echo "*******************"
    echo " "
    echo 'PLEASE DISABLE SWAP FROM FSTAB MANUALLY BY EDITING "etc/fstab" and commenting out the SWAP line'
    echo "The current joining address can be found in joining addr.txt or you can join using the command below."
    echo ""
    cat joining_addr.txt
    echo ""
    echo "*******************"
}

if [ $1 = update ]; then
    update
fi

if [ $2 = master ]; then
    cluster
fi

if [ $3 = gitlab ]; then
    gitlab
fi

finish