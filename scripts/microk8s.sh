wait() {
    sleep 5
    while true; do
        wait=$(kubectl get nodes | grep $(hostname) |  awk '{print $2;}')
        if [ $wait = Ready ]; then
            break
        fi
        sleep 1
    done
}

gitlab() {
    kubectl apply -f kubernetes-init/scripts/gitlab-admin-service-account.yaml
    kubectl get secret $(sudo kubectl get secrets | grep default-token |  awk '{print $1;}') -o jsonpath="{['data']['ca\.crt']}" | base64 --decode > gitlab-creds.txt
    kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}') > temp.txt
    cat temp.txt | grep token: >> gitlab-creds.txt
    rm temp.txt
    echo "*****GitLab******"
    echo ""
    cat gitlab-creds.txt
    echo ""
    echo "/*****GitLab******"
    kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}'
}

gitlab-prompt() {
    while true; do
        read -p "Install gitlab admin user?[Yy/Nn]  " yn </dev/tty
        case $yn in
            [Yy]* ) gitlab; break;;
            [Nn]* ) exit;;
            * ) echo "Please answer yes or no.";;
        esac
    done
}

sudo snap install microk8s --classic
sudo snap alias microk8s.kubectl kubectl
sudo microk8s.start
wait
sudo microk8s.enable dns dashboard
wait
kubectl label node $(hostname) kubernetes.io/role=master
kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.7.3/manifests/metallb.yaml
wait
kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.7.3/manifests/example-layer2-config.yaml
wait
kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.7.3/manifests/tutorial-2.yaml
echo '--allow-privileged=true' | sudo tee -a /var/snap/microk8s/current/args/kubelet
echo '--allow-privileged=true' | sudo tee -a /var/snap/microk8s/current/args/kube-apiserver
sudo systemctl restart snap.microk8s.daemon-kubelet.service
sudo systemctl restart snap.microk8s.daemon-apiserver.service
clear
figlet "absolutely proprietary"
gitlab-prompt