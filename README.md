# Kubernetes bootsrap for a Homelab
I grew tired of manually setting up VM's for my homelab cluster so I created this!
The script checks for old Docker installs and then updates the system accordingly.

## Bootstrap
`bash <(curl -L https://gitlab.com/janetsqy/kubernetes-init/raw/master/boot.sh)`

## Modes
There are currently 3 modes for the script
*  Kubernetes Core
*  k3s
*  microk8s

# Kubernetes Core
By default you have 3 options
*  Update
*  Master mode
*  GitLab admin setup

#### 1. Update
* Updates the system and installs Docker and Kubernetes core with kubeadm 

#### 2. Master mode
* Initiates the node as master
* Installas Metalb, Flannel and dashboard


#### 3. GitLab admin setup
* Install gitlab service token RBAC and prints out the certificates 

# k3s
By default you have 2 options
*  Install the system with the automated script
*  Install without traefik and other dependencies(but with MetalB and Flannel)

# microk8s
By default you have 2 options
* just one node(metal, dashboard,)
* gitlab admin installer